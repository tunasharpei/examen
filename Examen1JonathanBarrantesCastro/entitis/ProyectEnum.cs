﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen1JonathanBarrantesCastro.entitis
{

    public enum TypeOfVehicle
    {
        Car,
        Bus,
        Truck,

    }
        public enum TypeOftruck
        {
            BallastTractor = 1,
            ConcreteTransportTruck,
            Crane,
            DumpTruck,
            GarbageTruck,
            LogCarrier,
            RefrigeratorTruck,
            Trailer,
            TankTruck,
        }

        public enum TypeOfService
        {
            PublicTransport,
            Tourism,
            TransportingOfStudents,

        }

        public enum TypeOfAutomobile
        {
            Coupe,
            Hatchbak,
            PickUp,
            Sedan,
            SUV,
        }

        public enum BrandOfVehicle
        {
            Chevrolet,
            Honda,
            Hyundai,
            Kia,
            Mack,
            Mazda,
            MercedesBenz,
            Mitsubishi,
            Peugeot,
            Scania,
            Suzuki,
            Toyota,
            Volkswagen,
            Volvo,
        }

        public enum ColorOfVehicle
        {
            Aero,
            AmaranthPink,
            Blue,
            Bronze,
            CanaryYellow,
            FuchsiaPink,
            Green,
            Red,
            White,
        }
    
}
