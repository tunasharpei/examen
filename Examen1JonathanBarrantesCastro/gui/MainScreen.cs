﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Examen1JonathanBarrantesCastro.entitis;

namespace Examen1JonathanBarrantesCastro.gui
{
    public partial class frmMainScreen : Form
    {

        Dictionary<string, TypeOfVehicle> dictionaryTypeOfVehicle;
        Dictionary<string, TypeOftruck> dictionaryTypeOftruck;
        Dictionary<string, TypeOfService> dictionaryTypeOfService;
        Dictionary<string, TypeOfAutomobile> dictionaryTypeOfAutomobile;
        Dictionary<string, BrandOfVehicle> dictionaryBrandOfVehicle;
        Dictionary<string, ColorOfVehicle> dictionaryColorOfVehicle;


        public frmMainScreen()
        {
            InitializeComponent();

            //dictionaryColorOfVehicle = new Dictionary<string, ColorOfVehicle>();
            //dictionaryColorOfVehicle.Add("BallastTractor", ColorOfVehicle.Aero);
            //dictionaryColorOfVehicle.Add("BallastTractor", ColorOfVehicle.AmaranthPink);
            //dictionaryColorOfVehicle.Add("BallastTractor", ColorOfVehicle.Blue);
            //dictionaryColorOfVehicle.Add("BallastTractor", ColorOfVehicle.Bronze);
            //dictionaryColorOfVehicle.Add("BallastTractor", ColorOfVehicle.CanaryYellow);
            //dictionaryColorOfVehicle.Add("BallastTractor", ColorOfVehicle.FuchsiaPink);
            //dictionaryColorOfVehicle.Add("BallastTractor", ColorOfVehicle.Green);
            //dictionaryColorOfVehicle.Add("BallastTractor", ColorOfVehicle.Red);
            //dictionaryColorOfVehicle.Add("BallastTractor", ColorOfVehicle.White);

            //dictionaryBrandOfVehicle = new Dictionary<string, BrandOfVehicle>();
            //dictionaryBrandOfVehicle.Add("BallastTractor", BrandOfVehicle.Chevrolet);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.Honda);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.Hyundai);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.Kia);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.Mack);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.Mazda);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.MercedesBenz);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.Mitsubishi);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.Peugeot);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.Scania);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.Suzuki);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.Toyota);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.Volkswagen);
            //dictionaryBrandOfVehicle.Add("ConcreteTransportTruck", BrandOfVehicle.Volvo);



            dictionaryTypeOftruck = new Dictionary<string, TypeOftruck>();
            dictionaryTypeOftruck.Add("BallastTractor", TypeOftruck.BallastTractor);
            dictionaryTypeOftruck.Add("ConcreteTransportTruck", TypeOftruck.ConcreteTransportTruck);
            dictionaryTypeOftruck.Add("Crane", TypeOftruck.Crane);
            dictionaryTypeOftruck.Add("DumpTruck", TypeOftruck.DumpTruck);
            dictionaryTypeOftruck.Add("GarbageTruck", TypeOftruck.GarbageTruck);
            dictionaryTypeOftruck.Add("LogCarrier", TypeOftruck.LogCarrier);
            dictionaryTypeOftruck.Add("RefrigeratorTruck", TypeOftruck.RefrigeratorTruck);
            dictionaryTypeOftruck.Add("TankTruck", TypeOftruck.TankTruck);
            dictionaryTypeOftruck.Add("Trailer", TypeOftruck.Trailer);

            this.cbxTipoDeCamion.DisplayMember = "Key";
            this.cbxTipoDeCamion.ValueMember = "Value";
            this.cbxTipoDeCamion.DataSource = new BindingSource(dictionaryTypeOftruck, null);
            this.cbxTipoDeCamion.DropDownStyle = ComboBoxStyle.DropDownList;

            dictionaryTypeOfService = new Dictionary<string, TypeOfService>();
            dictionaryTypeOfService.Add("PublicTransport", TypeOfService.PublicTransport);
            dictionaryTypeOfService.Add("Tourism", TypeOfService.Tourism);
            dictionaryTypeOfService.Add("TransportingOfStudents", TypeOfService.TransportingOfStudents);

            this.cbxTipoDeServicio.DisplayMember = "Key";
            this.cbxTipoDeServicio.ValueMember = "Value";
            this.cbxTipoDeServicio.DataSource = new BindingSource(dictionaryTypeOfService, null);
            this.cbxTipoDeServicio.DropDownStyle = ComboBoxStyle.DropDownList;

            dictionaryTypeOfAutomobile = new Dictionary<string, TypeOfAutomobile>();
            dictionaryTypeOfAutomobile.Add("Coupe", TypeOfAutomobile.Coupe);
            dictionaryTypeOfAutomobile.Add("Hatchbak", TypeOfAutomobile.Hatchbak);
            dictionaryTypeOfAutomobile.Add("PickUp", TypeOfAutomobile.PickUp);
            dictionaryTypeOfAutomobile.Add("Sedan", TypeOfAutomobile.Sedan);
            dictionaryTypeOfAutomobile.Add("SUV", TypeOfAutomobile.SUV);

            this.cbxTipoDeAutomovil.DisplayMember = "Key";
            this.cbxTipoDeAutomovil.ValueMember = "Value";
            this.cbxTipoDeAutomovil.DataSource = new BindingSource(dictionaryTypeOfAutomobile, null);
            this.cbxTipoDeAutomovil.DropDownStyle = ComboBoxStyle.DropDownList;

            dictionaryTypeOfVehicle = new Dictionary<string, TypeOfVehicle>();
            dictionaryTypeOfVehicle.Add("Carro", TypeOfVehicle.Car);
            dictionaryTypeOfVehicle.Add("Bus", TypeOfVehicle.Bus);
            dictionaryTypeOfVehicle.Add("Camion", TypeOfVehicle.Truck);

            this.cbxTipoDeVehiculo.DisplayMember = "Key";
            this.cbxTipoDeVehiculo.ValueMember = "Value";
            this.cbxTipoDeVehiculo.DataSource = new BindingSource(dictionaryTypeOfVehicle, null);
            this.cbxTipoDeVehiculo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void frmMainScreen_Load(object sender, EventArgs e)
        {

        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
               
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
