﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Examen1JonathanBarrantesCastro.entitis;

namespace Examen1JonathanBarrantesCastro.logic
{
    class Car : Vehicle

    {
        private TypeOfAutomobile _typeOfAutomovile;
        private int numberOfDoors;

        public Car(TypeOfAutomobile _typeOfAutomovile, string registration, int model, BrandOfVehicle Brand, ColorOfVehicle Color, int passangerCapasity, double fiscalValue)
            : base(registration, model, Brand, Color, passangerCapasity, fiscalValue)
        {
            this._typeOfAutomovile = _typeOfAutomovile;
        }

        public int NumberOfDoors
        {
            get { return numberOfDoors; }
            set { numberOfDoors = value; }
        }



        public override double calculateInsurance()
        {
            throw new NotImplementedException();
        }

        private double calculateTaxForCar()
        {
            return 1;
        }
    }
}
