﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Examen1JonathanBarrantesCastro.entitis;


namespace Examen1JonathanBarrantesCastro.logic
{
    abstract class Vehicle
    {
        protected string _registration;
        protected int _model;
        protected BrandOfVehicle _Brand;
        protected ColorOfVehicle _Color;
        protected int _passangerCapasity;
        protected double _fiscalValue;

        public Vehicle(string registration, int model, BrandOfVehicle Brand, ColorOfVehicle Color, int passangerCapasity, double fiscalValue)
        {
            this._registration = registration;
            this._model = model;
            this._Brand = Brand;
            this._Color = Color;
            this._passangerCapasity = passangerCapasity;
            this._fiscalValue = fiscalValue;
                
        }

        public abstract double calculateInsurance();






    }
}
