﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Examen1JonathanBarrantesCastro.entitis;

namespace Examen1JonathanBarrantesCastro.logic
{
    class Bus : Vehicle
    {
        private TypeOfService _typeOfService;
        private int seatingCapacity;

        public Bus(TypeOfService typeOfService,string registration, int model, BrandOfVehicle Brand, ColorOfVehicle Color, int passangerCapasity, double fiscalValue) : base ( registration,  model,  Brand,  Color,  passangerCapasity,  fiscalValue)
        {
            this._typeOfService = typeOfService;
        }

        public int SeatingCapacity
        {
            get { return seatingCapacity; }
            set { seatingCapacity = value; }
        }
        

        public override double calculateInsurance()
        {
            throw new NotImplementedException();
        }

        private double calculateTaxForBus()
        {
            return 1;
        }
    }
}
