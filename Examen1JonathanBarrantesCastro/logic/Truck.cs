﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Examen1JonathanBarrantesCastro.entitis;

namespace Examen1JonathanBarrantesCastro.logic
{
    class Truck : Vehicle
    {
        private TypeOftruck _typeOfTruck;
        private int numberOfAxles;
                
        public Truck(TypeOftruck typeOfTruck, string registration, int model, BrandOfVehicle Brand, ColorOfVehicle Color, int passangerCapasity, double fiscalValue) : base ( registration,  model,  Brand,  Color,  passangerCapasity,  fiscalValue)
        {
            this._typeOfTruck = typeOfTruck;
        }

        public int NumberOfAxles
        {
            get { return numberOfAxles; }
            set { numberOfAxles = value; }
        }

        public override double calculateInsurance()
        {
            throw new NotImplementedException();
        }

        private double calculateTaxForTruck()
        {
            return 1;
        }
    }
}
